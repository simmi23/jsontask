	     var myapp=angular.module("myapp",[]);
		 var matches1 =  [
			    
  
                   {
                         "unique_id": 1185306,
                         "date": "2020-01-16T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-16T08:00:00.000Z",
                         "team-1": "South Africa",
                         "team-2": "England",
                         "type": "Test",
                         "squad": true,
                         "toss_winner_team": "England",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1187028,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T08:00:00.000Z",
                         "team-1": "India",
                         "team-2": "Australia",
                         "type": "ODI",
                         "squad": true,
                         "toss_winner_team": "Australia",
                         "winner_team": "India",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1208112,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T13:00:00.000Z",
                         "team-1": "Khulna Tigers",
                         "team-2": "Rajshahi Royals",
                         "type": "Twenty20",
                         "toss_winner_team": "Khulna Tigers",
                         "winner_team": "Rajshahi Royals",
                         "squad": false,
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1211921,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T08:30:00.000Z",
                         "team-1": "Chilaw Marians Cricket Club",
                         "team-2": "Sinhalese Sports Club",
                         "type": "Twenty20",
                         "toss_winner_team": "Sinhalese Sports Club",
                         "winner_team": "Chilaw Marians Cricket Club",
                         "squad": false,
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1211923,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T04:30:00.000Z",
                         "team-1": "Colts Cricket Club",
                         "team-2": "Ragama Cricket Club",
                         "type": "Twenty20",
                         "squad": true,
                         "toss_winner_team": "Colts Cricket Club",
                         "winner_team": "Ragama Cricket Club",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1211922,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T04:30:00.000Z",
                         "team-1": "Nondescripts Cricket Club",
                         "team-2": "Sri Lanka Army Sports Club",
                         "type": "Twenty20",
                         "squad": true,
                         "toss_winner_team": "Sri Lanka Army Sports Club",
                         "winner_team": "Nondescripts Cricket Club",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1211924,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T08:30:00.000Z",
                         "team-1": "Colombo Cricket Club",
                         "team-2": "Badureliya Sports Club",
                         "type": "Twenty20",
                         "squad": true,
                         "toss_winner_team": "Badureliya Sports Club",
                         "winner_team": "Colombo Cricket Club",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1197771,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T03:10:00.000Z",
                         "team-1": "Otago",
                         "team-2": "Auckland",
                         "type": "Twenty20",
                         "toss_winner_team": "Auckland",
                         "winner_team": "Auckland",
                         "squad": false,
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1195612,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T04:40:00.000Z",
                         "team-1": "Adelaide Strikers",
                         "team-2": "Brisbane Heat",
                         "type": "Twenty20",
                         "toss_winner_team": "Brisbane Heat",
                         "squad": true,
                         "winner_team": "Adelaide Strikers",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1204675,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T08:00:00.000Z",
                         "team-1": "South Africa Under-19s",
                         "team-2": "Afghanistan Under-19s",
                         "type": "YouthODI",
                         "squad": true,
                         "toss_winner_team": "South Africa Under-19s",
                         "winner_team": "Afghanistan Under-19s",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1213157,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T10:00:00.000Z",
                         "team-1": "Qatar Women",
                         "team-2": "Oman Women",
                         "type": "WomensT20I",
                         "squad": false,
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1213158,
                         "date": "2020-01-17T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-17T14:30:00.000Z",
                         "team-1": "Qatar Women",
                         "team-2": "Kuwait Women",
                         "type": "WomensT20I",
                         "squad": false,
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1194439,
                         "date": "2020-01-18T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-16T08:00:00.000Z",
                         "team-1": "Boland",
                         "team-2": "KwaZulu-Natal Inland",
                         "type": "First-class",
                         "squad": true,
                         "toss_winner_team": "Boland",
                         "matchStarted": true
                   },
                   {
                         "unique_id": 1194441,
                         "date": "2020-01-18T00:00:00.000Z",
                         "dateTimeGMT": "2020-01-16T08:00:00.000Z",
                         "team-1": "Gauteng",
                         "team-2": "Easterns",
                         "type": "First-class",
                         "squad": true,
                         "toss_winner_team": "Gauteng",
                         "matchStarted": true
                   },
                   {
                        "unique_id": 1194442,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T08:00:00.000Z",
                        "team-1": "Northerns",
                        "team-2": "Border",
                        "type": "First-class",
                        "squad": true,
                        "toss_winner_team": "Northerns",
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1194440,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T08:00:00.000Z",
                        "team-1": "South Western Districts",
                        "team-2": "Eastern Province",
                        "type": "First-class",
                        "squad": true,
                        "toss_winner_team": "South Western Districts",
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1210981,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T14:00:00.000Z",
                        "team-1": "Barbados",
                        "team-2": "Guyana",
                        "type": "First-class",
                        "toss_winner_team": "Barbados",
                        "squad": false,
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1210982,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T14:00:00.000Z",
                        "team-1": "Trinidad & Tobago",
                        "team-2": "Leeward Islands",
                        "type": "First-class",
                        "toss_winner_team": "Leeward Islands",
                        "squad": false,
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1210980,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T15:00:00.000Z",
                        "team-1": "Jamaica",
                        "team-2": "Windward Islands",
                        "type": "First-class",
                        "toss_winner_team": "Jamaica",
                        "squad": false,
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1211034,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-16T22:00:00.000Z",
                        "team-1": "New Zealand XI",
                        "team-2": "India A",
                        "type": "ListA",
                        "toss_winner_team": "New Zealand XI",
                        "winner_team": "India A",
                        "squad": false,
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1203677,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-18T22:00:00.000Z",
                        "team-1": "West Indies",
                        "team-2": "Ireland",
                        "type": "T20I",
                        "squad": true,
                        "matchStarted": false
                    },
                    {
                        "unique_id": 1195613,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-18T04:40:00.000Z",
                        "team-1": "Melbourne Stars",
                        "team-2": "Perth Scorchers",
                        "type": "Twenty20",
                        "toss_winner_team": "Melbourne Stars",
                        "squad": true,
                        "winner_team": "Melbourne Stars",
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1195614,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-18T08:10:00.000Z",
                        "team-1": "Sydney Thunder",
                        "team-2": "Sydney Sixers",
                        "type": "Twenty20",
                        "toss_winner_team": "Sydney Thunder",
                        "winner_team": "Sydney Thunder",
                        "squad": false,
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1211599,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-18T03:15:00.000Z",
                        "team-1": "Nepal Army Club",
                        "team-2": "Nepal Police Club",
                        "type": "ODI",
                        "squad": true,
                        "toss_winner_team": "Nepal Police Club",
                        "winner_team": "Nepal Army Club",
                        "matchStarted": true
                    },
                    {
                        "unique_id": 1194137,
                        "date": "2020-01-18T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                        "team-1": "Free State Women",
                        "team-2": "Gauteng Women",
                        "squad": false,
                        "matchStarted": true,
                        "type": ""
                    },
                    {
                       "unique_id": 1194136,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                       "team-1": "Kei Women",
                       "team-2": "South Western Districts Women",
                       "squad": false,
                       "matchStarted": true,
                       "type": ""
                   },
                   {
                       "unique_id": 1204679,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                       "team-1": "Australia Under-19s",
                       "team-2": "West Indies Under-19s",
                       "type": "YouthODI",
                       "squad": true,
                       "toss_winner_team": "West Indies Under-19s",
                       "winner_team": "West Indies Under-19s",
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1204676,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                       "team-1": "Bangladesh Under-19s",
                       "team-2": "Zimbabwe Under-19s",
                       "type": "YouthODI",
                       "squad": true,
                       "toss_winner_team": "Bangladesh Under-19s",
                       "winner_team": "Bangladesh Under-19s",
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1204678,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                       "team-1": "Canada Under-19s",
                       "team-2": "United Arab Emirates Under-19s",
                       "type": "YouthODI",
                       "squad": true,
                       "toss_winner_team": "United Arab Emirates Under-19s",
                       "winner_team": "United Arab Emirates Under-19s",
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1204677,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T08:00:00.000Z",
                       "team-1": "Japan Under-19s",
                       "team-2": "New Zealand Under-19s",
                       "type": "YouthODI",
                       "squad": true,
                       "toss_winner_team": "Japan Under-19s",
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1212541,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:00:00.000Z",
                       "team-1": "Zimbabwe",
                       "team-2": "Sri Lanka",
                       "type": "Test",
                       "squad": true,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1187029,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:00:00.000Z",
                       "team-1": "India",
                       "team-2": "Australia",
                       "type": "ODI",
                       "squad": true,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1211035,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T22:00:00.000Z",
                       "team-1": "New Zealand XI",
                       "team-2": "India A",
                       "type": "ListA",
                       "squad": true,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1213110,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T03:30:00.000Z",
                       "team-1": "India A Women",
                       "team-2": "Thailand Women",
                       "type": "Twenty20",
                       "toss_winner_team": "Thailand Women",
                       "winner_team": "Thailand Women",
                       "squad": false,
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1213109,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T03:30:00.000Z",
                       "team-1": "India B Women",
                       "team-2": "Bangladesh Women",
                       "type": "Twenty20",
                       "winner_team": "India B Women",
                       "toss_winner_team": "India B Women",
                       "squad": false,
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1213159,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T07:00:00.000Z",
                       "team-1": "Kuwait Women",
                       "team-2": "Oman Women",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1213160,
                       "date": "2020-01-18T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T12:00:00.000Z",
                       "team-1": "Qatar Women",
                       "team-2": "Oman Women",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": true
                   },
                   {
                       "unique_id": 1197804,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-18T23:40:00.000Z",
                       "team-1": "Auckland Women",
                       "team-2": "Wellington Women",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203678,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T22:00:00.000Z",
                       "team-1": "West Indies",
                       "team-2": "Ireland",
                       "type": "T20I",
                       "squad": true,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1195615,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T03:45:00.000Z",
                       "team-1": "Hobart Hurricanes",
                       "team-2": "Adelaide Strikers",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1195616,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:10:00.000Z",
                       "team-1": "Brisbane Heat",
                       "team-2": "Melbourne Renegades",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   { 
                       "unique_id": 1197772,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T03:10:00.000Z",
                       "team-1": "Auckland",
                       "team-2": "Wellington",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1211925,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:30:00.000Z",
                       "team-1": "Chilaw Marians Cricket Club",
                       "team-2": "Ragama Cricket Club",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1211926,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:30:00.000Z",
                       "team-1": "Colombo Cricket Club",
                       "team-2": "Nondescripts Cricket Club",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203547,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Assam",
                       "team-2": "Maharashtra",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203535,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Bengal",
                       "team-2": "Hyderabad (India)",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203543,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Chhattisgarh",
                       "team-2": "Tripura",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203536,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Delhi",
                       "team-2": "Vidarbha",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203549,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Goa",
                       "team-2": "Chandigarh",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203537,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Gujarat",
                       "team-2": "Punjab",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203539,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Himachal Pradesh",
                       "team-2": "Baroda",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203544,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Jharkhand",
                       "team-2": "Uttarakhand",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203538,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Kerala",
                       "team-2": "Rajasthan",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203540,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Madhya Pradesh",
                       "team-2": "Saurashtra",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203550,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Meghalaya",
                       "team-2": "Manipur",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1203541,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Mumbai",
                       "team-2": "Uttar Pradesh",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203551,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Nagaland",
                       "team-2": "Bihar",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203545,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Odisha",
                       "team-2": "Jammu & Kashmir",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203548,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Puducherry",
                       "team-2": "Arunachal Pradesh",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203546,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Services",
                       "team-2": "Haryana",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203552,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Sikkim",
                       "team-2": "Mizoram",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1203542,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T04:00:00.000Z",
                       "team-1": "Tamil Nadu",
                       "team-2": "Railways",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194359,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T07:30:00.000Z",
                       "team-1": "Boland",
                       "team-2": "KwaZulu-Natal Inland",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194361,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T07:30:00.000Z",
                       "team-1": "Gauteng",
                       "team-2": "Easterns",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                    },
					{
                       "unique_id": 1194362,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T07:30:00.000Z",
                       "team-1": "Northerns",
                       "team-2": "Border",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                    },
      
                    {
                       "unique_id": 1194360,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T07:30:00.000Z",
                       "team-1": "South Western Districts",
                       "team-2": "Eastern Province",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204681,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:00:00.000Z",
                       "team-1": "India Under-19s",
                       "team-2": "Sri Lanka Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204680,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T08:00:00.000Z",
                       "team-1": "Pakistan Under-19s",
                       "team-2": "Scotland Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213161,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T07:00:00.000Z",
                       "team-1": "Qatar Women",
                       "team-2": "Kuwait Women",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213162,
                       "date": "2020-01-19T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-19T12:00:00.000Z",
                       "team-1": "Kuwait Women",
                       "team-2": "Oman Women",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1195617,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T07:40:00.000Z",
                       "team-1": "Sydney Sixers",
                       "team-2": "Melbourne Stars",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1195618,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T10:40:00.000Z",
                       "team-1": "Perth Scorchers",
                       "team-2": "Sydney Thunder",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194285,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T08:00:00.000Z",
                       "team-1": "Dolphins",
                       "team-2": "Knights",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194286,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T08:00:00.000Z",
                       "team-1": "Titans",
                       "team-2": "Lions",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194284,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T08:00:00.000Z",
                       "team-1": "Warriors",
                       "team-2": "Cape Cobras",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204683,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T08:00:00.000Z",
                       "team-1": "Australia Under-19s",
                       "team-2": "Nigeria Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204682,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T08:00:00.000Z",
                       "team-1": "England Under-19s",
                       "team-2": "West Indies Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213111,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T03:30:00.000Z",
                       "team-1": "Bangladesh Women",
                       "team-2": "Thailand Women",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213112,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T03:30:00.000Z",
                       "team-1": "India A Women",
                       "team-2": "India B Women",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213163,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T07:00:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213164,
                       "date": "2020-01-20T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T12:00:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1195619,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T08:10:00.000Z",
                       "team-1": "Melbourne Renegades",
                       "team-2": "Hobart Hurricanes",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1211927,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T04:30:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1201365,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T23:00:00.000Z",
                       "team-1": "Australian Capital Territory Women",
                       "team-2": "Queensland Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                   },        
                   {
                       "unique_id": 1201364,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-20T23:00:00.000Z",
                       "team-1": "Tasmania Women",
                       "team-2": "Victoria Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                   },
                   {
                       "unique_id": 1201366,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T02:00:00.000Z",
                       "team-1": "Western Australia Women",
                       "team-2": "South Australia Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                   },
                   {
                       "unique_id": 1204684,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T08:00:00.000Z",
                       "team-1": "Bangladesh Under-19s",
                       "team-2": "Scotland Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1204685,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T08:00:00.000Z",
                       "team-1": "India Under-19s",
                       "team-2": "Japan Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1213165,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T10:00:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1213166,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T14:30:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "WomensT20I",
                       "squad": false,
                       "matchStarted": false
                    },
                   {
                       "unique_id": 1211036,
                       "date": "2020-01-21T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T22:00:00.000Z",
                       "team-1": "New Zealand A",
                       "team-2": "India A",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1195620,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T08:10:00.000Z",
                       "team-1": "Adelaide Strikers",
                       "team-2": "Melbourne Stars",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                   {
                       "unique_id": 1201367,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-21T23:00:00.000Z",
                       "team-1": "New South Wales Women",
                       "team-2": "Queensland Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                    },
                    {
                       "unique_id": 1213113,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T03:30:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1213114,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T03:30:00.000Z",
                       "team-1": "TBA",
                       "team-2": "TBA",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204687,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T08:00:00.000Z",
                       "team-1": "Afghanistan Under-19s",
                       "team-2": "United Arab Emirates Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204689,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T08:00:00.000Z",
                       "team-1": "New Zealand Under-19s",
                       "team-2": "Sri Lanka Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204688,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T08:00:00.000Z",
                       "team-1": "Pakistan Under-19s",
                       "team-2": "Zimbabwe Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204686,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T08:00:00.000Z",
                       "team-1": "South Africa Under-19s",
                       "team-2": "Canada Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1201369,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T23:00:00.000Z",
                       "team-1": "Australian Capital Territory Women",
                       "team-2": "New South Wales Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                    },
                    {
                       "unique_id": 1201368,
                       "date": "2020-01-22T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-22T23:00:00.000Z",
                       "team-1": "Tasmania Women",
                       "team-2": "Victoria Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                    },
                    {
                       "unique_id": 1195621,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T08:10:00.000Z",
                       "team-1": "Brisbane Heat",
                       "team-2": "Sydney Sixers",
                       "type": "Twenty20",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194444,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T08:00:00.000Z",
                       "team-1": "KwaZulu-Natal Inland",
                       "team-2": "North West",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1194443,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T08:00:00.000Z",
                       "team-1": "Western Province",
                       "team-2": "South Western Districts",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1209656,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T07:30:00.000Z",
                       "team-1": "Mashonaland Eagles",
                       "team-2": "Matabeleland Tuskers",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1209655,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T07:30:00.000Z",
                       "team-1": "Mid West Rhinos",
                       "team-2": "Rangers",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1210983,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T14:00:00.000Z",
                       "team-1": "Guyana",
                       "team-2": "Trinidad & Tobago",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1210984,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T15:00:00.000Z",
                       "team-1": "Jamaica",
                       "team-2": "Barbados",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1210985,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T17:30:00.000Z",
                       "team-1": "Windward Islands",
                       "team-2": "Leeward Islands",
                       "type": "First-class",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1201370,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T02:00:00.000Z",
                       "team-1": "Western Australia Women",
                       "team-2": "South Australia Women",
                       "squad": false,
                       "matchStarted": false,
                       "type": ""
                    },
                    {
                       "unique_id": 1204690,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T08:00:00.000Z",
                       "team-1": "Australia Under-19s",
                       "team-2": "England Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                    },
                    {
                       "unique_id": 1204691,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T08:00:00.000Z",
                       "team-1": "Nigeria Under-19s",
                       "team-2": "West Indies Under-19s",
                       "type": "YouthODI",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1211037,
                       "date": "2020-01-23T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-23T22:00:00.000Z",
                       "team-1": "New Zealand A",
                       "team-2": "India A",
                       "type": "ListA",
                       "squad": false,
                       "matchStarted": false
                   },
                   {
                       "unique_id": 1185307,
                       "date": "2020-01-24T00:00:00.000Z",
                       "dateTimeGMT": "2020-01-24T08:00:00.000Z",
                       "team-1": "South Africa",
                       "team-2": "England",
                       "type": "Test",
                       "squad": true,
                       "matchStarted": false
                   },
                   {
                        "unique_id": 1187677,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T07:00:00.000Z",
                        "team-1": "New Zealand",
                        "team-2": "India",
                        "type": "T20I",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1213058,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T13:30:00.000Z",
                        "team-1": "Pakistan",
                        "team-2": "Bangladesh",
                        "type": "T20I",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1195622,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T07:10:00.000Z",
                        "team-1": "Hobart Hurricanes",
                        "team-2": "Sydney Thunder",
                        "type": "Twenty20",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1195623,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T10:10:00.000Z",
                        "team-1": "Perth Scorchers",
                        "team-2": "Adelaide Strikers",
                        "type": "Twenty20",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1204693,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T08:00:00.000Z",
                        "team-1": "Afghanistan Under-19s",
                        "team-2": "Canada Under-19s",
                        "type": "YouthODI",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1204692,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T08:00:00.000Z",
                        "team-1": "Bangladesh Under-19s",
                        "team-2": "Pakistan Under-19s",
                        "type": "YouthODI",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1204694,
                        "date": "2020-01-24T00:00:00.000Z",
                        "dateTimeGMT": "2020-01-24T08:00:00.000Z",
                        "team-1": "India Under-19s",
                        "team-2": "New Zealand Under-19s",
                        "type": "YouthODI",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1187685,
                        "date": "2020-02-20T00:00:00.000Z",
                        "dateTimeGMT": "2020-02-20T22:30:00.000Z",
                        "team-1": "New Zealand",
                        "team-2": "India",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1187686,
                        "date": "2020-02-28T00:00:00.000Z",
                        "dateTimeGMT": "2020-02-28T22:30:00.000Z",
                        "team-1": "New Zealand",
                        "team-2": "India",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198232,
                        "date": "2020-06-04T00:00:00.000Z",
                        "dateTimeGMT": "2020-06-04T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "West Indies",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198233,
                        "date": "2020-06-12T00:00:00.000Z",
                        "dateTimeGMT": "2020-06-12T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "West Indies",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198234,
                        "date": "2020-06-25T00:00:00.000Z",
                        "dateTimeGMT": "2020-06-25T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "West Indies",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198241,
                        "date": "2020-07-30T00:00:00.000Z",
                        "dateTimeGMT": "2020-07-30T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "Pakistan",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198242,
                        "date": "2020-08-07T00:00:00.000Z",
                        "dateTimeGMT": "2020-08-07T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "Pakistan",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   },
                   {
                        "unique_id": 1198243,
                        "date": "2020-08-20T00:00:00.000Z",
                        "dateTimeGMT": "2020-08-20T10:00:00.000Z",
                        "team-1": "England",
                        "team-2": "Pakistan",
                        "type": "Test",
                        "squad": false,
                        "matchStarted": false
                   }
   ];
   
           /* function to sort the date*/
            matches1.sort(function(a, b){
                 return a.date - b.date;
		    });
		



		myapp.controller("jsondata",function ($scope) {
		      $scope.matches = matches1;
		});    
		
